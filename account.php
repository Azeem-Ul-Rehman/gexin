<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
<div class="theme-body">

    <?php include('includes/dark-header.php') ?>

    <div class="account-wrapper mobile">
        <div class="mobile-user-avatar">
            <div class="avatar-box">
                <div class="avatar-inner">
                    <img src="images/avatar.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="account-wrap-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="account-title ">تسجيل الدخول</div>
                        <a draggable="false" id="section-gexinid" class="account-item" href="user-id.php">
                            <div class="label">اسم المستخدم</div>
                            <div class="info text-ellipsis">TalhaAbbas</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-email" class="locked account-item" href="email.php" data-bs-toggle="modal" data-bs-target="#otpModal">
                            <div class="label">البريد الاكتورني</div>
                            <div class="info">
                                <div class="email">
                                    <div class="text-ellipsis">t*******@*******ook.com</div>
                                </div>
                            </div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-mobile-phone" class="locked account-item" href="phone.php">
                            <div class="label">رقم الهاتف</div>
                            <div class="info text-ellipsis ltr">-</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-linked" class="locked account-item" href="link-account.php">
                            <div class="label">ربط الحساب</div>
                            <div class="info connections">-</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-password" class="locked account-item" href="">
                            <div class="label">كلمه السر</div>
                            <div class="info text-ellipsis">*******</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-logout" class="account-item" href="login.php">
                            <div class="label">تسجيل الخروج</div>
                            <div class="info text-ellipsis">تسجيل الخروج من أجهزتك</div>
                            <div class="icon"></div>
                        </a>
                        <div class="account-title title-razer">تعريف الشخصيه</div>
                        <a draggable="false" id="section-personal-info" class="account-item" href="personal-info.php">
                            <div class="label">معلومات شخصيه</div>
                            <div class="info text-ellipsis">عربي , بشر</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-billing-address" class="locked account-item" href="billing-address.php">
                            <div class="label">العنوان</div>
                            <div class="info text-ellipsis">-</div>
                            <div class="icon"></div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-6">
                        <div class="account-title ">الامان</div>
                        <a draggable="false" id="section-recovery-email" class="locked account-item" href="email.php">
                            <div class="label">رموز الامان عبر الايمل</div>
                            <div class="info">-</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-tfa" class="locked account-item" href="authentication.php">
                            <div class="label">المصادقه الثنائيه</div>
                            <div class="info text-ellipsis">-</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-privacy-center" class="locked account-item" href="">
                            <div class="label">الخصوصيه</div>
                            <div class="info text-ellipsis">اعرف كيف يتم استخدام بيناتك</div>
                            <div class="icon"></div>
                        </a>
                        <a draggable="false" id="section-app-authorizations" class="locked account-item" href="">
                            <div class="label">برنامج المصادقه الثنائيه </div>
                            <div class="info text-ellipsis">تحكم ببرنامج المصادقه الثنائيه</div>
                            <div class="icon"></div>
                        </a>
                        <!-- <a draggable="false" id="section-delete-account" class="account-item" href="">
                            <div class="label">حذف الحساب</div>
                            <div class="info text-ellipsis">Permanently remove your account</div>
                            <div class="icon"></div>
                        </a> -->
                        <div class="account-title">التواصل</div>
                        <a draggable="false" id="section-notifications" class="account-item" href="">
                            <div class="label">الاشعارات و اخر الاخبار</div>
                            <div class="info text-ellipsis">اختار كيف تستمتع باخر الاخبار و العروض</div>
                            <div class="icon"></div>
                        </a>
                    </div>
                </div>
            </div>
            <?php include('includes/main-footer.php') ?>
        </div>
    </div>
</div>
<?php include ('includes/otp-modal.php') ?>
<?php include('includes/js.php') ?>
<script src="js/slick.min.js"></script>
<script>
    jQuery(document).ready(function ($) {
        $('.slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
                {
                    breakpoint: 400,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
        });
    });
</script>
<script>
    var checkboxEle = $(".itemCheck");
    checkboxEle.next().hide();
    checkboxEle.click(function () {
        var panelDiv = $(this).next();
        if ($(this).is(":checked")) {
            panelDiv.slideDown();
        } else {
            panelDiv.slideUp();
        }
    });
</script>

</body>
</html>