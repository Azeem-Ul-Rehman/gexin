<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body authentication-page rtl">
        <?php include ('includes/dark-header.php') ?>

        <div class="add-info-wrapper">
            <div class="add-info-inner">
                <div class="container">
                    <div class="link-wrap">
                        <div class="page-title">
                            <h3>المصادقه الثنائيه</h3>
                        </div>
                        <div class="two-step-main mobile">
                            <div class="general-info mt-4 mb-4">
                                <p style="direction:rtl">تتيح المصادقه الثنائيه زياده امان حسابك و اضافه طرق امنه لتعديل معلموماتك و استخدام المحفظه بامان</p>
                            </div>
                            <div class="input-label">الطرق المتاحه</div>
                            <div class="tfa-list">
                                <div class="tfa-item default null">
                                    <div class="label">عنوان البريد الإلكتروني</div>
                                    <div class="info"><div class="text-ellipsis">example@example.com</div></div>
                                </div>
                                <a href="security-authentication.php">
                                    <div class="tfa-item">
                                        <div class="label">برنامج المصادقه<span class="recommended text-green">(مفضل)</span></div>
                                        <div class="info">
                                            <div class="text-ellipsis"><span class="helper text-red">الاعداد الان</span></div>
                                        </div>
                                        <div class="icon"></div>
                                    </div>
                                </a>
                                <a href="security-number.php">
                                    <div class="tfa-item">
                                        <div class="label">رقم الهاتف</div>
                                        <div class="info">
                                            <div class="text-ellipsis"><span class="helper text-red">الاعداد الان</span></div>
                                        </div>
                                        <div class="icon"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="form-btns">
                                <a class="back" href="account.php">للخلف</a>
                            </div>
                        </div>
                        
                    </div>  
                </div>
            </div>
        </div>


        <?php include ('includes/main-footer.php') ?>
    </div>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
        var checkboxEle = $(".itemCheck");
        checkboxEle.next().hide();
        checkboxEle.click(function() {
            var panelDiv = $(this).next();
            if($(this).is(":checked")) {
                panelDiv.slideDown();
            } else {
                panelDiv.slideUp();
            }
        });
</script>
</body>
</html>