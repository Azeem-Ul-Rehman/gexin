<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body linking-account">
            <?php include ('includes/dark-header.php') ?>

            <div class="add-info-wrapper">
                <div class="add-info-inner">
                    <div class="container">
                        <div class="link-wrap">
                            <div class="page-title">
                                <h3>العنوان</h3>
                            </div>
                            <div class="acc-info-box">
                                <p>اضافه العنوان ستزيد من امان حسابك و تسهل تسوقك  </p>
                                <!-- <a href="">View your Razer Silver</a> -->
                            </div>
                            <div class="add-form add-billing-address mt-4">
                                <form method="POST" autocomplete="off">
                                    <div class="row">
                                        
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">الاسم الاول</div>
                                                    <input type="text" class="form-control" id="NewName" aria-describedby="NewEmail" placeholder="يوحنا">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">الاسم الاخير</div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="-">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">ألبلد</div>
                                                    <!-- <input type="text" class="form-control" id="" aria-describedby="" placeholder=""> -->
                                                    <select id="countries" class="js-states form-control">
                                                        <option>Java</option>
                                                        <option>Javascript</option>
                                                        <option>PHP</option>
                                                        <option>Visual Basic</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">الشارع </div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="عنوان الشارع ، ص. صندوق">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">رقم الشقه</div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="الشقة، الجناح، الوحدة، المبنى، الطابق">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">المدينه</div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="سنغافورة">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">المحافظه</div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="الولاية ، المقاطعة ، المنطقة ، المقاطعة">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="info-box mt-0">
                                                <div class="input-box new-mail">
                                                    <div class="input-holder">الرمز البريدي</div>
                                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="الرمز البريدي ، الرمز البريدي">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-btns">
                                                <a class="back" href="account.php">للخلف</a>
                                                <a class="next" href="account.php">التالي</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <?php include ('includes/main-footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    
    <script>
       $("#countries").select2({
          placeholder: "Location",
          allowClear: true
      });
</script>
</body>
</html>