<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/header.php') ?>

        <div class="checkout-wrap rtl">
            <div class="row justify-content-center">
                <div class="col-12 col-md-9 col-lg-8 col-xl-8">
                    <div class="checkout-wrap-inner">
                        <div class="row">
                            <div class="col-md-7 co-lg-7 col-xl-7 col-12">
                               <form action="">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="checkout-info-wrap">
                                                <h3>: بيانات التوصيل
                                                    <span> *بيانات مطلوبه</span>
                                                </h3>
                                                <p>We'll only use this information to contact you about your order</p>
                                                <div class="contact-details">
                                                    <div class="form-floating mb-4">
                                                        <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                                                        <label for="floatingInput">ألبريد الالكتروني </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="phone" name="phone" type="tel">
                                                    </div>
                                                </div>           
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shiupping-address-wrap">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5>عنوان التسوق</h5>
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="FirstName" placeholder="First Name">
                                                    <label for="FirstName">الاسم الاول</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Last Name">
                                                    <label for="floatingInput"> * الاسم الاخير</label>
                                                    <p>سيتم شحن الى مصر:  &nbsp;<span>United States</span></p>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Address Finder">
                                                    <label for="floatingInput">الشارع </label>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <h6>ادخل العنوان بالتوصيل</h6>
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Address Line 1*">
                                                    <label for="floatingInput">العنوان/ اسم الشارع 1*</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Address Line 2 (Optional)">
                                                    <label for="floatingInput">(العنوان) اسم الشارع *</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="رقم العماره , الفليلا , الدور  (المبنى)">
                                                    <label for="floatingInput">رقم الشقه</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Town/City*">
                                                    <label for="floatingInput">* المدينه</label>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-floating mb-4">
                                                    <div class="sect-box">
                                                        <select name="" id="">
                                                            <option selected>* المحافظه</option>
                                                            <option value="">Example</option>
                                                            <option value="">Example</option>
                                                            <option value="">Example</option>
                                                        </select>
                                                        <!-- <label for="floatingInput">Town/City*</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-floating mb-4">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="Zip Code">
                                                    <label for="floatingInput">الرقم البريدي</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="billing-address">
                                        <div class="row">
                                            <div class="col-12">
                                                <h4>Billing Address</h4>
                                                <div class="form-group checkbox-spec">
                                                    <input type="checkbox" onclick="showBilling();" id="Billing">
                                                    <label for="Billing">عنوان الشحن نفس عنوان الدفع</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="shopping-button">
                                                <button class="continue-btn">المتابعه الي طرق الشحن</button>
                                            </div>
                                        </div>
                                    </div>
                               </form>
                            </div>
                            <div class="col-12 col-md-5 col-lg-5 col-xl-5 position-relative">
                                <!-- <div class="order-cart-box-wrap"> -->
                                    <div class="order-informaion-box">
                                        <div class="order-info-inner">
                                            <h4>الطلب الخاص بك</h4>
                                            <div class="info-listing">
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="ItemsCounter">
                                                        <a class="accordion-button" data-bs-toggle="collapse" role="button" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            عنصر 1
                                                        </a>
                                                    </h2>
                                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="ItemsCounter" data-bs-parent="#accordionExample">
                                                        <div class="accordion-body">
                                                            <p>ألمنتج</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="info">
                                                    <div class="info-inner">
                                                        <div class="left-info">
                                                            <p>  مجموع قيمه الطلب</p>
                                                            <!-- <p>(Excludes Local Taxes)</p> -->
                                                        </div>
                                                        <div class="right-info">
                                                            <p>US $344.00</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="info">
                                                    <div class="info-inner">
                                                        <div class="left-info">
                                                            <p>الضريبه المضافه</p>
                                                        </div>
                                                        <div class="right-info">
                                                            <p>سيتم المحاسبه عند ادخال العنوان</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="info">
                                                    <div class="info-inner">
                                                        <div class="left-info">
                                                            <p>الشحن</p>
                                                        </div>
                                                        <div class="right-info">
                                                            <p>To be calculated after address entry</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="info">
                                                    <div class="info-inner total-info">
                                                        <div class="left-info">
                                                            <p>ألمجموع</p>
                                                        </div>
                                                        <div class="right-info">
                                                            <p>US $344.00</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php include ('includes/search-bar.php') ?>
    <?php include ('includes/footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>

    <script>
         // phone number select 
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
        autoHideDialCode: false,
        autoPlaceholder: "off",
        dropdownContainer: document.body,
        nationalMode: false,
        utilsScript: "build/js/utils.js",
    });
    </script>
        
</body>
</html>