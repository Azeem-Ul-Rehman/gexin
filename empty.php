<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/header.php') ?>

            <!-- Modal -->
            <div class="modal fade pricingModal" id="pricingModal" tabindex="-1" role="dialog" aria-labelledby="pricingModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="pricingModalLabel">Pricing Calculator</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="steps-system-wrap">
                            <div class="step-system-inner">
                                <div class="steps-info">
                                    <p>Step 1 of 9</p>
                                    <form id="msform">
                                        <!-- progressbar -->
                                        <ul id="progressbar" class="progressbar">
                                            <li class="active" id="service"></li>
                                            <li id="photography"></li>
                                            <li id="photoCount"></li>
                                            <li id="addOn"></li>
                                            <li id="homeArea"></li>
                                            <li id="orderVideo"></li>
                                            <li id="propertyInfo"></li>
                                            <li id="Scheudling"></li>
                                            <li id="auth"></li>
                                        </ul> 

                                        <!-- fieldsets -->
                                        <fieldset >
                                            <div class="form-card">
                                                <h2 class="fc-title">WHAT SERVICES DO YOU NEED?</h2>
                                                <div class="services-checklist">
                                                    <ul>
                                                        <li>
                                                            <span>
                                                                <span>01</span>
                                                                <p>Photos</p>
                                                                <div class="checkbox-wrap">
                                                                    <a href="">View Example</a>
                                                                    <div class="check-field">
                                                                        <input type="checkbox" id="photos">
                                                                        <label for="photos"></label>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span>
                                                                <span>02</span>
                                                                <p>Videos</p>
                                                                <div class="checkbox-wrap">
                                                                    <a href="">View Example</a>
                                                                    <div class="check-field">
                                                                        <input type="checkbox" id="videos">
                                                                        <label for="videos"></label>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span>
                                                                <span>03</span>
                                                                <p>Matterport</p>
                                                                <div class="checkbox-wrap">
                                                                    <a href="">View Example</a>
                                                                    <div class="check-field">
                                                                        <input type="checkbox" id="Matterport">
                                                                        <label for="Matterport"></label>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span>
                                                                <span>04</span>
                                                                <p>Zillow 3D Tour</p>
                                                                <div class="checkbox-wrap">
                                                                    <a href="">View Example</a>
                                                                    <div class="check-field">
                                                                        <input type="checkbox" id="zillow">
                                                                        <label for="zillow"></label>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span>
                                                                <span>05</span>
                                                                <p>Floor Plans</p>
                                                                <div class="checkbox-wrap">
                                                                    <a href="">View Example</a>
                                                                    <div class="check-field">
                                                                        <input type="checkbox" id="plans">
                                                                        <label for="plans"></label>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- style  -->
                                        <fieldset >
                                            <div class="form-card">
                                                <h2 class="fs-title">WHAT STYLE PHOTOGRAPHY?</h2>
                                                <div class="price-style-wrap">
                                                    <div class="row">
                                                        <div class="col-md-6 col-lg-6 col-xl-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="standard">
                                                                <label class="for-checkbox-tools" for="standard" data-checked-value="standard">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <h4>STANDARD</h4>
                                                                    <div class="style-img">
                                                                        <img src="images/blog-img.png" alt="">
                                                                        <div class="search-icon">
                                                                            <a href="">
                                                                                <i class="fa fa-search"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="features">
                                                                        <ul>
                                                                            <li>Most popular</li>
                                                                            <li>HDR photography</li>
                                                                            <li>Blue sky enhancement</li>
                                                                            <li>Great value</li>
                                                                        </ul>
                                                                        <div class="priceforthis">
                                                                            <h5>Price: <span>$00.00</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-6 col-xl-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="luxury">
                                                                <label class="for-checkbox-tools" for="luxury" data-checked-value="luxury">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <h4>STANDARD</h4>
                                                                    <div class="style-img">
                                                                        <img src="images/room.png" alt="">
                                                                        <div class="search-icon">
                                                                            <a href="">
                                                                                <i class="fa fa-search"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="features">
                                                                        <ul>
                                                                            <li>Highest quality</li>
                                                                            <li>Clear Window Views</li>
                                                                            <li>Green Grass</li>
                                                                            <li>Enhancement (if necessary)</li>
                                                                        </ul>
                                                                        <div class="priceforthis">
                                                                            <h5>Price: <span>$00.00</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 

                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- photos  -->
                                        <fieldset>
                                            <div class="form-card">
                                                <h2 class="fs-title">HOW MANY PHOTOS WOULD YOU LIKE?</h2>
                                                <div class="photo-count-wrap">
                                                    <div class="row">
                                                        <div class="col-md-4 col-lg-4 col-xl-4">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="15photo">
                                                                <label class="for-checkbox-tools" for="15photo" data-checked-value="15photo">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="photo-count-box">
                                                                       <div class="img-icon-box">
                                                                            <div class="img-icon">
                                                                                <img src="images/photo-icon.svg" alt="">
                                                                            </div>
                                                                       </div>
                                                                        <div class="img-count">
                                                                            <h5>15</h5>
                                                                            <p>Photos</p>
                                                                        </div>
                                                                        <div class="stat-tag">
                                                                            <p>Popular</p>
                                                                        </div>
                                                                        <div class="priceforthis">
                                                                            <h5>Price: <span>$00.00</span></h5>
                                                                        </div>  
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-4 col-xl-4">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="25photo">
                                                                <label class="for-checkbox-tools" for="25photo" data-checked-value="25photo">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="photo-count-box">
                                                                       <div class="img-icon-box">
                                                                            <div class="img-icon">
                                                                                <img src="images/photo-icon.svg" alt="">
                                                                            </div>
                                                                       </div>
                                                                        <div class="img-count">
                                                                            <h5>25</h5>
                                                                            <p>Photos</p>
                                                                        </div>
                                                                        <div class="stat-tag">
                                                                            <p>Popular</p>
                                                                        </div>
                                                                        <div class="priceforthis">
                                                                            <h5>Price: <span>$00.00</span></h5>
                                                                        </div>  
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-4 col-xl-4">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="40photo">
                                                                <label class="for-checkbox-tools" for="40photo" data-checked-value="40photo">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="photo-count-box">
                                                                       <div class="img-icon-box">
                                                                            <div class="img-icon">
                                                                                <img src="images/photo-icon.svg" alt="">
                                                                            </div>
                                                                       </div>
                                                                        <div class="img-count">
                                                                            <h5>40</h5>
                                                                            <p>Photos</p>
                                                                        </div>
                                                                        <div class="stat-tag">
                                                                            <p>Popular</p>
                                                                        </div>
                                                                        <div class="priceforthis">
                                                                            <h5>Price: <span>$00.00</span></h5>
                                                                        </div>  
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 

                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- add on services  -->
                                        <fieldset>
                                            <div class="form-card">
                                                <h2 class="fs-title">ADD ON SERVICES</h2>
                                                <div class="addon-services-wrap">
                                                    <div class="row">
                                                        <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="checkbox" name="tools" id="aerial">
                                                                <label class="for-checkbox-tools" for="aerial" data-checked-value="aerial">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="addon-service">
                                                                        <div class="title">
                                                                            <p>AERIAL PHOTOGRAPHY</p>
                                                                        </div>
                                                                        <div class="view-example-btn">
                                                                            <a href="">VIEW EXAMPLE</a>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="checkbox" name="tools" id="dusk">
                                                                <label class="for-checkbox-tools" for="dusk" data-checked-value="dusk">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="addon-service">
                                                                        <div class="title">
                                                                            <p>DUSK PHOTOGRAPHY</p>
                                                                        </div>
                                                                        <div class="view-example-btn">
                                                                            <a href="">VIEW EXAMPLE</a>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="checkbox" name="tools" id="virtual">
                                                                <label class="for-checkbox-tools" for="virtual" data-checked-value="virtual">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="addon-service">
                                                                        <div class="title">
                                                                            <p>VIRTUAL DUSK</p>
                                                                        </div>
                                                                        <div class="view-example-btn">
                                                                            <a href="">VIEW EXAMPLE</a>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="checkbox" name="tools" id="aerialdusk">
                                                                <label class="for-checkbox-tools" for="aerialdusk" data-checked-value="aerialdusk">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <div class="addon-service">
                                                                        <div class="title">
                                                                            <p>AERIAL DUSK PHOTOGRAPHY</p>
                                                                        </div>
                                                                        <div class="view-example-btn">
                                                                            <a href="">VIEW EXAMPLE</a>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 

                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- home area size  -->
                                        <fieldset>
                                            <div class="form-card">
                                                <h2 class="fs-title">APPROXIMATELY HOW LARGE IS THE HOME?</h2>
                                                <div class="area-selection-wrap">
                                                    <div class="slider">
                                                        <div class="style-item">
                                                            <input class="checkbox-tools" type="radio" name="tools" id="area2000fs">
                                                            <label class="for-checkbox-tools" for="area2000fs" data-checked-value="area2000fs">
                                                                <div class="check-mark">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="area-selection-box">
                                                                    <div class="img-icon-box">
                                                                        <div class="img-icon">
                                                                            <img src="images/photo-icon.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="area">
                                                                        <h5>2000 SF</h5>
                                                                    </div>
                                                                    <!-- <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>   -->
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="style-item">
                                                            <input class="checkbox-tools" type="radio" name="tools" id="area2500fs">
                                                            <label class="for-checkbox-tools" for="area2500fs" data-checked-value="area2500fs">
                                                                <div class="check-mark">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="area-selection-box">
                                                                    <div class="img-icon-box">
                                                                        <div class="img-icon">
                                                                            <img src="images/photo-icon.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="area">
                                                                        <h5>2500 SF</h5>
                                                                    </div>
                                                                    <!-- <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>   -->
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="style-item">
                                                            <input class="checkbox-tools" type="radio" name="tools" id="area3000fs">
                                                            <label class="for-checkbox-tools" for="area3000fs" data-checked-value="area3000fs">
                                                                <div class="check-mark">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="area-selection-box">
                                                                    <div class="img-icon-box">
                                                                        <div class="img-icon">
                                                                            <img src="images/photo-icon.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="area">
                                                                        <h5>3000 SF</h5>
                                                                    </div>
                                                                    <!-- <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>   -->
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="style-item">
                                                            <input class="checkbox-tools" type="radio" name="tools" id="area3500fs">
                                                            <label class="for-checkbox-tools" for="area3500fs" data-checked-value="area3500fs">
                                                                <div class="check-mark">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="area-selection-box">
                                                                    <div class="img-icon-box">
                                                                        <div class="img-icon">
                                                                            <img src="images/photo-icon.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="area">
                                                                        <h5>3500 SF</h5>
                                                                    </div>
                                                                    <!-- <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>   -->
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 

                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- video  -->
                                        <fieldset >
                                            <div class="form-card">
                                                <h2 class="fs-title">FOR ALL ORDERS WITH VIDEO</h2>
                                                <div class="order-video-wrap">
                                                    <div class="row">
                                                        <div class="col-md-6 col-lg-6 col-xl-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="agentFeature">
                                                                <label class="for-checkbox-tools" for="agentFeature" data-checked-value="agentFeature">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <h4>ADD AGENT FEATURE</h4>
                                                                    <div class="style-img">
                                                                        <img src="images/blog-img.png" alt="">
                                                                        <div class="search-icon">
                                                                            <a href="">
                                                                                <i class="fa fa-search"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-6 col-xl-6">
                                                            <div class="style-item">
                                                                <input class="checkbox-tools" type="radio" name="tools" id="addAerial">
                                                                <label class="for-checkbox-tools" for="addAerial" data-checked-value="addAerial">
                                                                    <div class="check-mark">
                                                                        <i class="fa fa-check"></i>
                                                                    </div>
                                                                    <h4>ADD AERIAL</h4>
                                                                    <div class="style-img">
                                                                        <img src="images/room.png" alt="">
                                                                        <div class="search-icon">
                                                                            <a href="">
                                                                                <i class="fa fa-search"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="priceforthis">
                                                                        <h5>Price: <span>$00.00</span></h5>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 

                                                    <div class="current-price">
                                                        <p>Current Price:</p>
                                                        <h5>$5,000</h5>
                                                    </div>
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- property info  -->
                                        <fieldset >
                                            <div class="form-card">
                                                <h2 class="fs-title">Property information</h2>
                                                <div class="property-info-wrap">
                                                    <div class="price-modal-form">
                                                        <div class="form-group">
                                                            <label for="propertyAddress">Property Address</label>
                                                            <input type="text" class="form-control" id="propertyAddress" placeholder="Enter Property Location / Address" name="propertyAddress">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="additionalInfo">ADDITIONAL INFORMATION</label>
                                                            <textarea type="text" class="form-control" id="additionalInfo" placeholder="Ex: lock box code, homeowners will be present, contact assistant at 555-1234 etc" name="additionalInfo"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="shots">Must have shots</label>
                                                            <textarea type="text" class="form-control" id="shots" placeholder="Ex: Expansive backyard, park across street, open floor plan, updated feature in home, etc" name="shots"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- schedule date  -->
                                        <fieldset >
                                            <div class="form-card">
                                                <h2 class="fs-title">Scheudling</h2>
                                                <div class="scheduling-wrap">
                                                    <div class="price-modal-form">
                                                        <div class="row">
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="scheduledate">Schedule Date</label>
                                                                    <input type="date" name="scheduledate" id="scheduledate" placeholder="Schedule Date">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="scheduletime">Anytime</label>
                                                                    <select name="cars" id="scheduletime" class="anytime">
                                                                        <option value="volvo">Volvo</option>
                                                                        <option value="saab">Saab</option>
                                                                        <option value="mercedes">Mercedes</option>
                                                                        <option value="audi">Audi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="name">Your Name</label>
                                                                    <input type="text" class="form-control user" id="name" placeholder="Your Name" name="name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="email">Email</label>
                                                                    <input type="email" class="form-control email" id="email" placeholder="Email" name="email">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="phone">Phone</label>
                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group itemWrapper">
                                                                    <label for="homeowner">homeowner info</label>
                                                                    <input class="itemCheck" type="checkbox" value="1" />
                                                                    <div class="showContainer" style="display:none">
                                                                        <div class="row inner-field">
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="name">Homeowner Name</label>
                                                                                    <input type="text" class="form-control user" id="name" placeholder="Homeowner Name" name="name">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="phone">Phone</label>
                                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="emailowner">
                                                                        <div class="check-field">
                                                                            <input type="checkbox" id="photos">
                                                                            <label for="photos">Send email confirmation to the homeowner.</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group itemWrapper">
                                                                    <label for="homeowner">Orderinig on behalf of</label>
                                                                    <input class="itemCheck" type="checkbox" value="1" />
                                                                    <div class="showContainer" style="display:none">
                                                                        <div class="row inner-field">
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="pname">Person's name</label>
                                                                                    <input type="text" class="form-control user" id="pname" placeholder="Person's Name" name="pname">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="phone">Phone</label>
                                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="bname">Brokerage Name</label>
                                                                                    <input type="text" class="form-control user" id="bname" placeholder="Brokerage Name" name="bname">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="bname">City</label>
                                                                                    <input type="text" class="form-control city" id="City" placeholder="City" name="City">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- login / register  -->

                                        <fieldset >
                                            <div class="form-card">
                                                <!-- <h2 class="fs-title"></h2> -->
                                                <div class="scheduling-wrap">
                                                    <div class="price-modal-form">
                                                        <div class="row">
                                                            <div class="col-md-12 col-12">
                                                                <div class="auth-titles">
                                                                    <h3>Login</h3>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="email">Email</label>
                                                                    <input type="email" class="form-control email" id="email" placeholder="Email" name="email">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="loginpassword">Password</label>
                                                                    <input type="password" class="form-control pass" id="loginpassword" placeholder="Password" name="loginpassword">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <div class="or">
                                                                    <h5>Or</h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <div class="auth-titles">
                                                                    <h3>Register</h3>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="fname">First Name</label>
                                                                    <input type="text" class="form-control user" id="fname" placeholder="First Name" name="fname">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="lname">Last Name</label>
                                                                    <input type="text" class="form-control user" id="lname" placeholder="Last Name" name="lname">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="email">Email</label>
                                                                    <input type="email" class="form-control email" id="email" placeholder="Email" name="email">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="phone">Phone</label>
                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="bname">Brokerage Name</label>
                                                                    <input type="text" class="form-control user" id="bname" placeholder="Brokerage Name" name="bname">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="loginpassword">Password</label>
                                                                    <input type="password" class="form-control pass" id="loginpassword" placeholder="Password" name="loginpassword">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="form-group">
                                                                    <label for="loginpassword">Password</label>
                                                                    <input type="password" class="form-control pass" id="loginpassword" placeholder="Password" name="loginpassword">
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-12">
                                                                <div class="form-group itemWrapper">
                                                                    <label for="homeowner">homeowner info</label>
                                                                    <input class="itemCheck" type="checkbox" value="1" />
                                                                    <div class="showContainer" style="display:none">
                                                                        <div class="row inner-field">
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="name">Homeowner Name</label>
                                                                                    <input type="text" class="form-control user" id="name" placeholder="Homeowner Name" name="name">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="phone">Phone</label>
                                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group itemWrapper">
                                                                    <label for="homeowner">Orderinig on befalf of</label>
                                                                    <input class="itemCheck" type="checkbox" value="1" />
                                                                    <div class="showContainer" style="display:none">
                                                                        <div class="row inner-field">
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="pname">Person's name</label>
                                                                                    <input type="text" class="form-control user" id="pname" placeholder="Person's Name" name="pname">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="phone">Phone</label>
                                                                                    <input type="phone" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pl-0">
                                                                                <div class="form-group">
                                                                                    <label for="bname">Brokerage Name</label>
                                                                                    <input type="text" class="form-control user" id="bname" placeholder="Brokerage Name" name="bname">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-12 pr-0">
                                                                                <div class="form-group">
                                                                                    <label for="bname">City</label>
                                                                                    <input type="text" class="form-control city" id="City" placeholder="City" name="City">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->


                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="pricing-buttons">
                                                <div class="pricing-buttons-inner">
                                                    <input type="button" name="previous" class=" previous action-button-previous" value="GO BACK" /> 
                                                    <input type="button" name="next" class="next action-button" value="Continue" />
                                                </div>
                                            </div>
                                        </fieldset>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <div class="pricing-buttons">
                            <div class="pricing-buttons-inner">
                                <input type="button" name="previous" class="prev-step action-button" value="Previous" />

                                    <div class="current-price">
                                        <p>Current Price:</p>
                                        <h5>$5,000</h5>
                                    </div>
                                <input type="button" name="next" class="next action-button" value="cONTINUE" />
                            </div>
                        </div>
                    </div> -->
                    </div>
                </div>
            </div>


        <?php include ('includes/footer.php') ?>
    </div>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
         jQuery(document).ready(function($) {
            $('.slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                arrows: true,
                responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 400,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }]
            });
        });
    </script>
    <script>
        var checkboxEle = $(".itemCheck");
        checkboxEle.next().hide();
        checkboxEle.click(function() {
            var panelDiv = $(this).next();
            if($(this).is(":checked")) {
                panelDiv.slideDown();
            } else {
                panelDiv.slideUp();
            }
        });
</script>
</body>
</html>