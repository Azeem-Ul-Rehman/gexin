<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/header.php') ?>

           <section>
               <div class="main-banner-wrap">
                   <div class="main-banner-inner">
                       <div class="banner-img has-icon" style="background-image: url(images/banner-desktop.jpg);">
                        <img src="images/inbox.png" class="main-banner-icon" alt="">
                        <h1>البريد الوارد</h1>
                    </div>
                   </div>
               </div>
           </section>

           <section>
               <div class="table-wrapper">
                    <div class="container">
                        <div class="table-wrap-inner">
                            <div class="title-wrap">
                                <h2> ألبريد الوارد</h2>
                                <div class="results-info">
                                    <div id="ResultInfo_Results" class="Results ">
                                        Results 
                                        <span>0</span> - <span>0</span> of <span>0</span>        
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="d-none d-md-block d-lg-block d-xl-block"> تاريخ الطلب</th>
                                        <th scope="col" class="mb-result">العنوان </th>
                                        <th scope="col" class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">رقم التذكره</th>
                                        <th scope="col" class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">الحاله</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <a href="ticket-inbox.php">
                                        <tr>
                                            <!-- <td>No record found</td> -->
                                            <td class=" d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">6-12-2020</td>
                                            <td class="result mb-result"><a href="ticket-inbox.php">gx6434494940ytj8</a></td>
                                            <td class=" d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">192-456-90</td>
                                            <td class=" d-none d-md-table-cell d-lg-table-cell d-xl-table-cell" >closed</td>
                                        </tr>
                                    </a>
                                </tbody>
                            </table>
                        </div>
                   </div>
               </div>
           </section>


    </div>

    <?php include ('includes/search-bar.php') ?>
    <?php include ('includes/footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    
</body>
</html>