<nav class="navbar navbar-expand-lg navbar-dark dark-header" style="direction:ltr">
  <div class="container">
    <a class="navbar-brand" href="account.php">
        <img src="images/logo-icon.svg" alt="">
        <span>اسم المستخدم </span>
    </a>
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
    </button> -->
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">الحساب </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">الضمان</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">من نحن </a>
        </li>
        
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          العربية
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item active" href="#">العربية</a></li>
            <li><a class="dropdown-item" href="#">English</a></li>
            <li><a class="dropdown-item" href="#">Deutsch</a></li>
            <li><a class="dropdown-item" href="#">繁體中文 (Hong Kong SAR)</a></li>
            <li><a class="dropdown-item" href="#">繁體中文 (Hong Kong SAR)</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>