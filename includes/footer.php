<footer>
    <div class="footer-wrap rtl">
        <div class="container footer-content">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12 col-12">
                    <div class="footer-links-wrap">
                        <div class="row">
                            <div class="col-md-7 col-lg-8 col-xl-8 col-12 pe-0 ps-0">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-xl-4 col-12">
                                        <div class="footer-links-box">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button d-none d-md-none d-lg-block d-xl-block cursor-default" >
                                                اكتشف الموقع
                                                </button>
                                                <button class="accordion-button media-vcenter d-block d-md-block d-lg-none d-xl-none" type="button" data-bs-toggle="collapse" data-bs-target="#explore" aria-expanded="true" aria-controls="collapseOne">
                                                اكتشف الموقع
                                                </button>
                                                </h2>
                                                <div id="explore" class="accordion-collapse collapse show customHideShow" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <ul>
                                                            <li>
                                                                <a href="">توب اب </a>
                                                            </li>
                                                            <li>
                                                                <a href="">بطاقات العاب </a>
                                                            </li>
                                                            <li>
                                                                <a href="">اجهزه الالعاب</a>
                                                            </li>
                                                            <li>
                                                                <a href="">اعاده شحن الارصده</a>
                                                            </li>
                                                            <li>
                                                                <a href="">الاكسسورات</a>
                                                            </li>
                                                            <li>
                                                                <a href="">اخر الاخبار</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xl-4 col-12">
                                        <div class="footer-links-box">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button d-none d-md-none d-lg-block d-xl-block cursor-default" >
                                                الدعم 
                                                </button>
                                                <button class="accordion-button media-vcenter d-block d-md-block d-lg-none d-xl-none" type="button" data-bs-toggle="collapse" data-bs-target="#support" aria-expanded="true" aria-controls="collapseOne">
                                                الدعم 
                                                </button>
                                                </h2>
                                                <div id="support" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <ul>
                                                            <li>
                                                                <a href="">املا تذكره</a>
                                                            </li>
                                                            <li>
                                                                <a href="">عناوين المتاجر</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xl-4 col-12">
                                        <div class="footer-links-box">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button d-none d-md-none d-lg-block d-xl-block cursor-default" >
                                                الشركه 
                                                </button>
                                                <button class="accordion-button media-vcenter d-block d-md-block d-lg-none d-xl-none" type="button" data-bs-toggle="collapse" data-bs-target="#company" aria-expanded="true" aria-controls="collapseOne">
                                                الشركه 
                                                </button>
                                                </h2>
                                                <div id="company" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample" id="refine-search">
                                                    <div class="accordion-body">
                                                        <ul>
                                                            <li>
                                                                <a href="">من نحن </a>
                                                            </li>
                                                            <li>
                                                                <a href="">اتصل بنا</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 col-lg-4 col-xl-4 col-12">
                                <div class="social-wrap">
                                    <h5> من اجلك عالم العاب بكل ماتحتاجه من بطاقات و ارصده</h5>
                                    <div class="social-acc">
                                        <div class="social-acc-list">
                                            <i class="fa fa-facebook"></i>
                                            <i class="fab fa-instagram"></i>
                                            <i class="fab fa-twitter"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-term">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="terms">
                                    <span>Copyright © 2021 Gexin Inc. All rights reserved</span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="tc-links">
                                    <ul>
                                        <li>
                                            <a href="">Terms of Service </a>
                                        </li>
                                        <li>
                                            <a href="">Legal Terms</a>
                                        </li>
                                        <li>
                                            <a href=""> Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a href=""> Cookie Policy </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>