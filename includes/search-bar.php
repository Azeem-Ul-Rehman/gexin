<div class="global-search-overlay" id="showOrHide" style="display:none;">
  <div class="global-search__background"></div>
    <div class="global-search__content">
        <div class="global-search__embed">
            <div class="global-search__searchbar row d-flex justify-content-center">
                <div class="col-8 col-md-7 col-lg-7 col-xl-7">
                    <div class="input-group" style="direction:ltr">
                        <span class="input-group-text" id="basic-addon1">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                        <span class="cross-btn">
                            <i class="fa fa-times" onclick="showOrHideDiv()"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>