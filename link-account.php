<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body linking-account">
            <?php include ('includes/dark-header.php') ?>

            <div class="add-info-wrapper">
                <div class="add-info-inner">
                    <div class="container">
                        <div class="link-wrap">
                            <div class="page-title">
                                <h3>الدخول عبر الحسابات </h3>
                            </div>
                            <div class="page-description">
                                <p>استخدام حساباتك للدخول الي جيكسن بكل سهوله</p>
                            </div>

                            <div class="accounts-list">
                                <ul>
                                    <li>
                                        <div class="row align-items-center">
                                            <div class="col-md-1 col-lg-1 col-xl-1 col-2 ps-0">
                                                <div class="social-icon">
                                                    <img src="images/facebook.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-lg-9 col-xl-9 col-6">
                                                <div class="social-name">
                                                    <p>Facebook</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-lg-2 col-xl-2 col-4">
                                                <div class="connect-btn">
                                                    <button>ربط</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row align-items-center">
                                            <div class="col-md-1 col-lg-1 col-xl-1 col-2 ps-0">
                                                <div class="social-icon">
                                                    <img src="images/google.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-lg-9 col-xl-9 col-6">
                                                <div class="social-name">
                                                    <p>Google</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-lg-2 col-xl-2 col-4">
                                                <div class="connect-btn">
                                                    <button>ربط</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row align-items-center">
                                            <div class="col-md-1 col-lg-1 col-xl-1 col-2 ps-0">
                                                <div class="social-icon">
                                                    <img src="images/twitch.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-lg-9 col-xl-9 col-6">
                                                <div class="social-name">
                                                    <p>Twitch</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-lg-2 col-xl-2 col-4">
                                                <div class="connect-btn">
                                                    <button>ربط</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="form-btns">
                                <a class="back" href="account.php">للخلف</a>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <?php include ('includes/main-footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
         jQuery(document).ready(function($) {
            $('.slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                arrows: true,
                responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 400,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }]
            });
        });
    </script>
    <script>
        var checkboxEle = $(".itemCheck");
        checkboxEle.next().hide();
        checkboxEle.click(function() {
            var panelDiv = $(this).next();
            if($(this).is(":checked")) {
                panelDiv.slideDown();
            } else {
                panelDiv.slideUp();
            }
        });
</script>
</body>
</html>