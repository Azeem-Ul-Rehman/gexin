<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
<div class="theme-body">
    <?php include('includes/top-head.php') ?>
    <div class="inner-section login-inner" style="background-image:url(images/login-banner.jpg)">
        <div class="auth-portal-wrap" >
            <div class="auth-portal-inner">
                <div class="auth-portal-form-wrap" id="addLoading">
                    <div class="auth-form-box login-for-box">
                        <form action="" id="login-form">
                            <div class="title" dir="rtl" style='direction:rtl;'>تسجيل الدخول</div>
                            <div class="row">
                                <div class="col-12 ps-0 pe-0">
                                    <div class="form-group">
                                        <input type="email" id="email" placeholder="بريد الالكتروني"
                                               style='direction:rtl;'>
                                    </div>
                                </div>
                                <div class="col-12 ps-0 pe-0">
                                    <div class="form-group" id="show_hide_password">
                                        <input class="pass" type="password" id="password" placeholder="كلمه السر"
                                               style='direction:rtl;'>
                                        <div class="input-group-addon">
                                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 ps-0 pe-0">
                                    <div class="forgot-pass">
                                        <p><a href="" style='direction:rtl;'>هل نسيت كلمه السر</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="submit-button">
                                <a href="javascript:void(0)" class="border-gradient login-btn">تسجيل الدخول </a>
                            </div>
                            <div class="ssi">
                                <div class=" helper"><span class="text">تسجيل الدخول ب</span></div>
                                <div class="items ">
                                    <div class="item-out border-gradient">
                                        <span class="fb item " title="Facebook" alt="Facebook"></span>
                                    </div>
                                    <div class="item-out border-gradient">
                                        <span class="gplus item " title="Google" alt="Google"></span>
                                    </div>
                                    <div class="item-out border-gradient">
                                        <span class="twitch item " title="Twitch" alt="Twitch"></span>
                                    </div>
                                </div>
                                <div class="buttons-login-container">
                                    <a draggable="false" id="btn-register"
                                       class="btn btn-outline-gray btn-block border-gradient" href="">تسجيل حساب
                                        جديد</a>
                                </div>
                            </div>

                        </form>
                        <div class="loading-box d-none">
                            <div class="loading-box-inner">
                                <h2 class="text-white">جاري تسجيل الدخول</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- <footer class="footer-main">
                <p><a href="#" target="_blank" class="gexin-com hover-opacity" rel="">Gexin.com</a></p>
                <p>Copyright © 2021 Gexin Inc. All rights reserved. <a href=""
                                                                       target="_blank">FAQ</a> | <a href=""
                                                                                                    target="_blank">Legal
                        Terms</a> | <a
                            href="" target="_blank"
                            rel="noopener noreferrer">Privacy Policy</a> | <a href=""
                                                                              target="_blank" rel="noopener noreferrer">Cookie
                        Policy</a></p>
            </footer> -->
        </div>
        <?php include('includes/main-footer.php') ?>
    </div>

</div>
<?php include('includes/js.php') ?>
<script src="js/slick.min.js"></script>
</body>
</html>