<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body linking-account">
            <?php include ('includes/dark-header.php') ?>

            <div class="add-info-wrapper mobile-responsive">
                <div class="add-info-inner">
                    <div class="container">
                        <div class="link-wrap">
                            <div class="page-title">
                                <h3>معلومات شخصية</h3>
                            </div>
                            <div class="add-form">
                                <form method="POST" autocomplete="off">
                                    <!-- <div class="input-label mb-15">ahahhahahahah </div> -->
                                    <div class="page-description">
                                        <p>لا يتم مشاركة هذه المعلومات علنًا.</p>
                                    </div>


                                        <div class="desk-form">
                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <input name="firstName" type="text" class="form-control-2   " placeholder="John" maxlength="255" spellcheck="false" autocomplete="off" value="">
                                                    <div class="input-placeholder">الاسم الأول</div>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <input name="firstName" type="text" class="form-control-2   " placeholder="Doe" maxlength="255" spellcheck="false" autocomplete="off" value="">
                                                    <div class="input-placeholder">الكنية</div>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <select class="js-select2">
                                                        <option>النوع</option>
                                                        <option>Male </option>
                                                        <option>Female</option>
                                                    </select>
                                                    <div class="input-placeholder"> جنس </div>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <div class="row w-100">
                                                        <div class="col-4 p-0">
                                                            <select class="js-select2">
                                                                <option>يوم</option>
                                                                <option>1 </option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-4 p-0">
                                                            <select class="js-select2">
                                                                <option>شهر</option>
                                                                <option>Jan </option>
                                                                <option>Feb</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-4 p-0">
                                                            <select class="js-select2">
                                                                <option>عام</option>
                                                                <option>1990 </option>
                                                                <option>1991</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="input-placeholder">تاريخ الولادة</div>
                                                </label>
                                            </div>

                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <select class="js-select2">
                                                        <option>عربي </option>
                                                        <option>English</option>
                                                        <option>French</option>
                                                    </select>
                                                    <div class="input-placeholder"> لغة </div>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="input-container-2 ">
                                                    <select class="js-select2">
                                                        <option>أفغانستان </option>
                                                        <option>ألبانيا</option>
                                                        <option>الجزائر</option>
                                                    </select>
                                                    <div class="input-placeholder"> موقع </div>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="mbl-form">
                                            <div class="col-12">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="يوحنا">
                                                    <label for="floatingInput">الاسم الأول</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="يوحنا">
                                                    <label for="floatingInput">الكنية</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-3 form-selects">
                                                    <select class="js-select2">
                                                        <option>Human</option>
                                                        <option>Male </option>
                                                        <option>Female</option>
                                                    </select>
                                                    <label for="floatingInput">جنس</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-3 form-selects">
                                                    <div class="row w-100">
                                                        <div class="col-4 p-0">
                                                                <select class="js-select2">
                                                                    <option>اليوم</option>
                                                                    <option>1 </option>
                                                                    <option>2</option>
                                                                </select>
                                                        </div>
                                                        <div class="col-4 p-0">
                                                            <select class="js-select2">
                                                                <option>الشهر</option>
                                                                <option>Jan </option>
                                                                <option>Feb</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-4 p-0">
                                                            <select class="js-select2">
                                                                <option>السنه</option>
                                                                <option>1990 </option>
                                                                <option>1991</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label for="floatingInput"> تاريخ الولادة</label>
                                                </div>
                                                <!-- <div class="form-floating mb-3 form-selects">
                                                    <select class="js-select2">
                                                        <option>Human</option>
                                                        <option>Male </option>
                                                        <option>Female</option>
                                                    </select>
                                                </div> -->
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-3 form-selects">
                                                    <select class="js-select2">
                                                        <option>العربيه </option>
                                                        <option>English</option>
                                                        <option>French</option>
                                                    </select>
                                                    <label for="floatingInput"> لغة</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-floating mb-3 form-selects">
                                                    <select class="js-select2">
                                                        <option>Afghanistan </option>
                                                        <option>Albania</option>
                                                        <option>Algeria</option>
                                                    </select>
                                                    <label for="floatingInput"> موقع</label>
                                                </div>
                                            </div>
                                            

                                            <div class="col-12">
                                                <div class="form-floating mb-3">
                                                    <input type="text" class="form-control" id="floatingInput" placeholder="John">
                                                    <label for="floatingInput">الاسم الأول</label>
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="form-btns">
                                            <a class="back" href="account.php">للخلف</a>
                                            <a class="next" href="account.php">التالي</a>
                                        </div>
                                </form>
                            </div>
                            
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <?php include ('includes/main-footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
    $(document).ready(function() {

        $(".js-select2").select2();
        
        $(".js-select2-multi").select2();

        $(".large").select2({
            dropdownCssClass: "big-drop",
        });
        
    });
    </script>
</body>
</html>