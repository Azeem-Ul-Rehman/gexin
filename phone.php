<!DOCTYPE html>
<html lang="arabic">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body linking-account">
            <?php include ('includes/dark-header.php') ?>

            <div class="add-info-wrapper rtl">
                <div class="add-info-inner">
                    <div class="container">
                        <div class="link-wrap">
                            <div class="page-title">
                                <h3>الهاتف</h3>
                            </div>
                            <div class="add-form">
                                <form method="POST" autocomplete="off">
                                    <div class="input-label mb-15">تغير رقم الهاتف </div>
                                    <div class="page-description">
                                        <p>بمجرد اضافه رقم الهاتف سيتم استخدامه في المصادقه الثنائيه</p>
                                    </div>
                                        <div class="select-phone">
                                            <div class="row">
                                                <div class="col-md-4 col-4 ps-0 pe-0">
                                                    <div class="input-placeholder">رقم الهاتف</div>
                                                </div>
                                                <div class="col-md-7 col-7 ps-0 pe-0">
                                                    <input id="phone" name="phone" type="tel">
                                                </div>
                                                <div class="col-md-1 col-1 ps-0 pe-0">
                                                    <div class="input-remove">
                                                        <button><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-btns">
                                            <a class="back" href="account.php">للخلف</a>
                                            <a class="next" href="account.php">التالي</a>
                                        </div>
                                </form>
                            </div>
                            
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <?php include ('includes/main-footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
         // phone number select 
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
        // allowDropdown: false,
        autoHideDialCode: false,
        autoPlaceholder: "off",
        dropdownContainer: document.body,
        //   excludeCountries: ["us"],
        //   formatOnDisplay: false,
        //   geoIpLookup: function(callback) {
        //     $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //       var countryCode = (resp && resp.country) ? resp.country : "";
        //       callback(countryCode);
        //     });
        //   },
        //   hiddenInput: "full_number",
        //   initialCountry: "auto",
        //   localizedCountries: { 'de': 'Deutschland' },
        nationalMode: false,
        //   onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        // separateDialCode: true,
        utilsScript: "build/js/utils.js",
    });
    </script>
    <script>
        var checkboxEle = $(".itemCheck");
        checkboxEle.next().hide();
        checkboxEle.click(function() {
            var panelDiv = $(this).next();
            if($(this).is(":checked")) {
                panelDiv.slideDown();
            } else {
                panelDiv.slideUp();
            }
        });
</script>
</body>
</html>