<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
<div class="theme-body">
    <?php include('includes/header.php') ?>

    <section>
        <div class="main-banner-wrap">
            <div class="main-banner-inner">
                <div class="banner-img" style="background-image: url(images/banner-desktop.jpg);">
                    <img src="images/logo-icon.png" class="main-banner-logo" alt="">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="reload-wrap mb-4">
            <div class="container">
                <div class="reload-inner row bg--gray-translucent border-radius-large pt-20 justify-content-center">
                    <div class="col-sm-12 col-md-11 col-xl-8">
                        <div class="text-center">
                            <h1> اعاده تعبئه رصيد الخاص بك</h1>
                            <hr class="dark-med-grey hr--30">
                        </div>
                        <div>
                            <div class="row justify-content-center rtl">
                                <div class="col">
                                    <div>
                                        <div class="card mb-3">
                                            <div class="card-body">
                                                <p class="mt-0 mb-1 media-row__header__title text--secondary">رصيد
                                                    محفظتك</p>
                                                <div class="media bg__content--dark border-radius-small p-3">
                                                    <div class="media-aside align-self-center">
                                                        <div class="wallet-balance-avatar">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="media-body overflow-hidden ml-3">
                                                        <p class="no-margin text--truncate">johndoe@example.com</p>
                                                        <div class="d-flex flex-row flex-wrap">
                                                            <div class="media img-icon mr-4">
                                                                <div class="media-body">
                                                                    <div class="media-body">
                                                                        <span class="text--zgold">0</span>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex mr-icon align-self-center">
                                                                    <img src="images/zgold.png" alt="">
                                                                </div>
                                                            </div>
                                                            <!-- <div class="media img-icon">
                                                                <div class="d-flex mr-icon align-self-center">
                                                                    <img src="images/zsilver.png" alt="">
                                                                </div>
                                                                <div class="media-body">
                                                                    <div class="text--zsilver">500</div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row justify-content-center">
                                <div class="col">
                                    <div class="payment-methods-selection-wrap">
                                        <div class="payment-method-inner">
                                            <div class="method-header">
                                                <h5><span>1</span>اختار طريقة الدفع</h5>
                                            </div>
                                            <div class="methods-lsiting" style="direction:ltr">
                                                <form action="" id="msform">
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <div class="add-method-wrap">
                                                                <div class="row">
                                                                    <!-- <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="dollar">
                                                                            <label class="for-checkbox-tool"
                                                                                   for="dollar"
                                                                                   data-checked-value="dollar">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/dollargeneral.jpg"
                                                                                         alt="">
                                                                                </div>
                                                                            </label>
                                                                            <p>Dollar General</p>
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="dollar">
                                                                            <label class="for-checkbox-tools"
                                                                                   for="dollar"
                                                                                   data-checked-value="dollar">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/dollargeneral.jpg"
                                                                                         alt="">
                                                                                </div>
                                                                                <p>Dollar General</p>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="goldVoucher">
                                                                            <label class="for-checkbox-tools"
                                                                                   for="goldVoucher"
                                                                                   data-checked-value="goldVoucher">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/gold-voucher.png"
                                                                                         alt="">
                                                                                </div>
                                                                                <p>Bonus Gold Voucher</p>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="Paypal">
                                                                            <label class="for-checkbox-tools"
                                                                                   for="Paypal"
                                                                                   data-checked-value="Paypal">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/paypal.png" alt="">
                                                                                </div>
                                                                                <p>Paypal</p>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="paysafecard">
                                                                            <label class="for-checkbox-tools"
                                                                                   for="paysafecard"
                                                                                   data-checked-value="paysafecard">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/paysafecard.png"
                                                                                         alt="">
                                                                                </div>
                                                                                <p>paysafecard</p>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-lg-3 col-xl-3 col-6">
                                                                        <div class="style-item">
                                                                            <input class="checkbox-tools"
                                                                                   type="radio" name="tools"
                                                                                   id="Diners">
                                                                            <label class="for-checkbox-tools"
                                                                                   for="Diners"
                                                                                   data-checked-value="Diners">
                                                                                <div class="check-mark">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div class="add-method">
                                                                                    <img src="images/payment-diners.png"
                                                                                         alt="">
                                                                                </div>
                                                                                <p>Diners</p>
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5 dollar" style="display: none">
                            <div class="row justify-content-center">
                                <div class="col">
                                    <div class="reload-step mb-3 mt-10 card--numbered">
                                        <div class="method-header">
                                            <h5><span>1</span>اختار طريقة الدفع</h5>
                                        </div>
                                        <div class="amount-card-body">
                                            <div class="reload-back-wrap">
                                                <div class="pt-20 pb-20">
                                                    <div class="reload-gold-amount d-flex justify-content-center">
                                                        <div class="media img-icon img-icon--medium media--vcenter">
                                                            <div class="media-body">
                                                                <div class="text--zgold text--lg" id="inputValueRefect">
                                                                    0
                                                                </div>
                                                            </div>
                                                            <div class="media-aside align-self-center">
                                                                <img src="images/zgold.png" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3">
                                                        <div class="form-group">
                                                            <input type="text" onkeypress="return onlyNumberKey(event)"
                                                                   placeholder="كمية" id="inputValueChange" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center cta">
                                                <a href=""> <span><i class="fa fa-info"></i></span>كيفيه الشحن بكود
                                                    بطاقه جيكسن </a>
                                            </div>
                                            <!-- <form action="">
                                                <div class="save-payment" style="direction:ltr">
                                                    <div class="form-checkbox">
                                                        <input type="checkbox" id="vsvr">
                                                        <label for="vsvr">Virtual Stage Vacant Room</label>
                                                    </div>
                                                </div>
                                            </form> -->
                                        </div>
                                        <div class="card-footer">
                                            <div class="text-center btn-grp justify-content-center">
                                                <a href="" class="primary-btn button">يلغي </a>
                                                <a href="javascript:void(0)" class="secondary-btn button" id="nextbtn">التالي </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5 goldVoucher" style="display:none;">
                            <div class="row justify-content-center">
                                <div class="col">
                                    <div class="reload-step mb-3 mt-10 card--numbered">
                                        <div class="method-header">
                                            <h5><span>1</span>اختار طريقة الدفع</h5>
                                        </div>
                                        <div class="amount-card-body">
                                            <div class="reload-back-wrap">
                                                <div class="pt-20 pb-20">
                                                    <div class="reload-gold-amount d-flex justify-content-center">
                                                        <!-- <div class="media img-icon img-icon--medium media--vcenter">
                                                            <div class="media-aside align-self-center">
                                                                <img src="images/zgold.png" alt="">
                                                            </div>
                                                            <div class="media-body">
                                                                <div class="text--zgold text--lg">0</div>
                                                            </div>
                                                        </div> -->
                                                        <div class="media-body-voucher">
                                                            <p> ادخل كود البطاقه </p>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3">
                                                        <div class="form-group">
                                                            <input type="text" id="voucher"
                                                                   placeholder="رقم التعريف الشخصي أو رمز القسيمة">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center cta">
                                                <a href=""> <span><i class="fa fa-info"></i></span> كيفيه الشحن بالفيزا</a>
                                            </div>
                                            <!-- <form action="">
                                                <div class="save-payment">
                                                    <div class="form-checkbox">
                                                        <input type="checkbox" id="vsvr">
                                                        <label for="vsvr">Virtual Stage Vacant Room</label>
                                                    </div>
                                                </div>
                                            </form> -->
                                        </div>
                                        <div class="card-footer">
                                            <div class="text-center btn-grp justify-content-center">
                                                <a href="" class="primary-btn button">يلغي </a>
                                                <a href="javascript:void(0)" class="secondary-btn button"
                                                   id="nextbtnNew">التالي </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-5 section-otp" style="display: none">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="opt-pass-outer">
                                        <div class="otp-pass ">
                                            <div class="icon-wrap">
                                                <div class="icon">
                                                    <img src="images/lock-white.svg" alt="" id="imgSource">
                                                </div>
                                            </div>
                                            <div class="title">
                                                <h5>المصادقة بخطوتين</h5>
                                            </div>
                                            <div class="otp-body rtl">
                                                <h5>لتحسين الأمان ، أنت مطالب الآن بإدخال رمز للمتابعة</h5>
                                                <p>أدخل الرمز الذي تم إنشاؤه بواسطة المصدق الخاص بك </p>
                                                <div class="input-group-otp" style="direction:ltr">
                                                    <form class="otc" name="one-time-code" action="#" id="otc-1">
                                                        <fieldset>
                                                            <!-- <legend>Validation Code</legend> -->
                                                            <label for="otc-1">Number 1</label>
                                                            <label for="otc-2">Number 2</label>
                                                            <label for="otc-3">Number 3</label>
                                                            <label for="otc-4">Number 4</label>
                                                            <label for="otc-5">Number 5</label>
                                                            <label for="otc-6">Number 6</label>
                                                            <div>
                                                                <input type="number" pattern="[0-9]*" value=""
                                                                       inputtype="numeric" autocomplete="one-time-code"
                                                                       id="otc-1" required>

                                                                <!-- Autocomplete not to put on other input -->
                                                                <input type="number" pattern="[0-9]*" min="0" max="9"
                                                                       maxlength="1" value="" inputtype="numeric"
                                                                       id="otc-2" required>
                                                                <input type="number" pattern="[0-9]*" min="0" max="9"
                                                                       maxlength="1" value="" inputtype="numeric"
                                                                       id="otc-3" required>
                                                                <input type="number" pattern="[0-9]*" min="0" max="9"
                                                                       maxlength="1" value="" inputtype="numeric"
                                                                       id="otc-4" required>
                                                                <input type="number" pattern="[0-9]*" min="0" max="9"
                                                                       maxlength="1" value="" inputtype="numeric"
                                                                       id="otc-5" required>
                                                                <input type="number" pattern="[0-9]*" min="0" max="9"
                                                                       maxlength="1" value="" inputtype="numeric"
                                                                       id="otc-6" required>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                                <div class="buttons-modal">
                                                    <button type="button" class="btn btn-secondary cancel-btn"
                                                            data-bs-dismiss="modal">يلغي
                                                    </button>
                                                </div>
                                                <div class="modal-tButtons">
                                                    <a href="" class="cancel-btn">يلغي</a>
                                                    <a href="" class="new-code">رمز جديد</a>
                                                </div>
                                                <div class="sending-options">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>اختر طريقة مختلفة</option>
                                                        <option value="1">رسالة نصية</option>
                                                        <option value="2">رموز النسخ الاحتياطي</option>
                                                        <option value="3">تحتاج مساعدة</option>
                                                    </select>
                                                    <!-- <div class="dropdown">
                                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                        اختر طريقة مختلفة
                                                        </a>

                                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <li><a class="dropdown-item" href="#">رسالة نصية</a></li>
                                                            <li><a class="dropdown-item" href="#">رموز النسخ الاحتياطي</a></li>
                                                            <li><a class="dropdown-item" href="#">تحتاج مساعدة</a></li>
                                                        </ul>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <span class="t_c">By proceeding with the order, you agree to Gexin's <a href=""
                                                                                                                target="_blank"
                                                                                                                place="">Terms of Service</a> and <a
                                                    href="" target="_blank"
                                                    place="privacy_link">Privacy Policy</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
<?php include('includes/otp-modal.php') ?>
<?php include('includes/search-bar.php') ?>
<?php include('includes/footer.php') ?>
<?php include('includes/js.php') ?>
<script src="js/slick.min.js"></script>

<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

</body>
</html>