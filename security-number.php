<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body authentication-page ">
        <?php include ('includes/dark-header.php') ?>

        <div class="add-info-wrapper rtl">
            <div class="add-info-inner">
                <div class="container">
                    <div class="link-wrap">
                        <div class="page-title">
                            <h3>المصادقه الثنائيه</h3>
                        </div>
                        <div class="security-wrap">
                            <div class="row">
                                <div class="col-md-7 col-lg-7 col-xl-7">
                                    <div class="input-label ps-0 mb-3 mt-4">اختار الطريقه</div>
                                    <div class="authentication-label">
                                        <h5 class="ps-3 fs-14">رقم الهاتف</h5>
                                        <div class="info">+923078779883</div>
                                    </div>
                                    <div class="input-label ps-0 mb-2 mt-2">الاختيرات</div>
                                    <div class="changing-options">
                                        <label class=" custom-radio">
                                            <input name="option" type="radio" value="primary">
                                            <span class="radio"></span>
                                            <div class="info">
                                                <div class="title">جعل هذه الطريقه المفضلى</div>
                                                <div class="description">سوف يصلك الكود الامان باستخدام رقم الهاتف</div>
                                            </div>
                                        </label>
                                        <label class=" custom-radio">
                                            <input name="option" type="radio" value="edit">
                                            <span class="radio"></span>
                                            <div class="info">
                                                <div class="title">تعديل رقم الهاتف </div>
                                                <div class="description">تعديل رقم الهاتف المسجل لدينا</div>
                                            </div>
                                        </label>
                                        <label class=" custom-radio">
                                            <input name="option" type="radio" value="edit">
                                            <span class="radio"></span>
                                            <div class="info">
                                                <div class="title">حذف الطريقه</div>
                                                <div class="description">لن يمكنك استخدام هذه الطريقه في وسائل الامان الخاصه بك</div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-btns">
                            <a class="back" href="authentication.php">للخلف</a>
                            <a class="next" href="account.php">التالي</a>
                        </div>
                        
                    </div>  
                </div>
            </div>
        </div>


        <?php include ('includes/main-footer.php') ?>
    </div>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    
</body>
</html>