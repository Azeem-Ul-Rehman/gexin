<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/header.php') ?>

           <section>
               <div class="main-banner-wrap">
                   <div class="main-banner-inner">
                       <div class="banner-img has-icon" style="background-image: url(images/banner-desktop.jpg);">
                        <img src="images/inbox.png" class="main-banner-icon" alt="">
                        <h1>انشأ تذكره</h1>
                    </div>
                   </div>
               </div>
           </section>

           <section>
               <div class="submit-ticket-wrap">
                    <div class="container">
                        <div class="submit-ticket-inner">
                                <div class="form-wrap">
                                    <div class="form-desclaimer">
                                        <p>تعاني مشاكل من شحن محفظه جيكسن الخاصه بك او بطلب الشحن الخاصه بك املا تذكره للتواصل مع الدعم الفني</p>
                                    </div>
                                    <div class="form-box">
                                        <h3>!أرسل لنا رسالة</h3>
                                        
                                        <h4>البريد الإلكتروني</h4>
                                        <p><a href="mailto: info@gexinstore.com">info@gexinstore.com</a></p>
                                        <form action="">
                                            <div class="mb-3">
                                                <label for="nature" class="form-label"><span>*</span>طبيعة الطلب</label>
                                                <select class="form-select" aria-label="">
                                                    <option selected>---</option>
                                                    <option value="1">حساب جيكسن الخاص بك</option>
                                                    <option value="2">شجن لعبه</option>
                                                    <option value="3">كود بطاقه الشحن لايعمل</option>
                                                    <option value="3">اخرى</option>
                                                </select>
                                            </div>
                                            <div class="mb-3">
                                                <label for="nature" class="form-label"><span>*</span>نوع الطلب</label>
                                                <select class="form-select" aria-label="">
                                                    <option selected>---</option>
                                                    <option value="1">لدي مشاكل بكلمه المرور و تسجيل الدخول</option>
                                                    <option value="2">لدي مشاكل بكلمه برمز الامان و المصادقه الثنائيه</option>
                                                    <option value="3">لدي مشكله تقنيه خاصه بالموقع</option>
                                                    <option value="3">ارغب بتقديم اقتراح </option>
                                                    <option value="4">ارغب في ان اكون بائع معتمد او موزع معتمد</option>
                                                </select>
                                            </div>
                                            <div class="mb-3">
                                                <label for="nature" class="form-label"><span>*</span>عنوان</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="mb-3">
                                                <label for="" class="form-label">وصف  <span>*</span>(قم بتضمين معرّف المعاملة أو إيصالات الدفع ، إذا كان ذلك متاحًا.)</label>
                                                <textarea name="" id="" cols="30" rows="10"></textarea>
                                                <div class="bottom-txt">من خلال توفير مزيد من المعلومات حول استفسارك ، يمكننا التحقيق في استفسارك بشكل أسرع.</div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="" class="form-label"><span>*</span> <strong>مرفق (ق)</strong> </label>
                                                <input type="file" id="myFile" class="fileInput" name="filename">
                                            </div>

                                            <div class="confirm-button">
                                                <button class="cnfrm-btn">confirm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
               </div>
           </section>


    </div>
    <?php include ('includes/search-bar.php') ?>
    <?php include ('includes/footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    
</body>
</html>