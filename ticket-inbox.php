<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/header.php') ?>

           <section>
               <div class="main-banner-wrap">
                   <div class="main-banner-inner">
                       <div class="banner-img has-icon" style="background-image: url(images/banner-desktop.jpg);">
                        <img src="images/inbox.png" class="main-banner-icon" alt="">
                        <h1>ألبريد الوارد</h1>
                    </div>
                   </div>
               </div>
           </section>

           <div class="ticket-wrapper rtl">
               <div class="row justify-content-center ">
                   <div class="col-12 col-md-8 col-lg-7 col-xl-6">
                       <div class="ticket-inner-wrap">
                           <div class="ticket-req-title">
                               <h1>العنوان: <span>Sample Text Here</span></h1>
                           </div>
                           <div class="ticket-req-info">
                               <div class="communication-history">
                                   <h3>تاريخ الاتصال</h3>
                                   <div class="history-box">
                                       <div class="history-top">
                                            <div class="title">
                                                <h4>التواصل عبر</h4>
                                            </div>
                                            <div class="time">19-Nov-2021 12:11 PM</div>
                                       </div>
                                       <div class="history-bottom">
                                            <div class="history-desc">
                                                <p>hahahahha candscsdakcascksdkcansdckaskdcn</p>
                                            </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="add-info-wrap">
                                    <div class="add-info-inner">
                                        <h4>المزيد من التفاصيل</h4>
                                        <div class="add-info-listing">
                                            <div class="add-info">
                                                <span>البريد </span>
                                                <div class="info-dataValue">
                                                    <a href="malto:info@gexinstore.com">info@gexinstore.com</a>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>عنوان التذكره </span>
                                                <div class="info-dataValue">
                                                   <p>211119-003089</p>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>الحاله </span>
                                                <div class="info-dataValue">
                                                    <p>جديد</p>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>تم انشاء التذكره بتاريخ </span>
                                                <div class="info-dataValue">
                                                    <p>19-Nov-2021 12:11 PM</p>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>تم تحديث التذكره بتاريخ </span>
                                                <div class="info-dataValue">
                                                    <p>19-Nov-2021 12:11 PM</p>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>المنتج </span>
                                                <div class="info-dataValue">
                                                    <ul>
                                                        <li>ذهب</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="add-info">
                                                <span>تفاصيل المنتج </span>
                                                <div class="info-dataValue">
                                                    <ul>
                                                        <li>ذهب</li>
                                                        <li>لعبة / شراء الخدمة</li>
                                                        <li>بيجو لايف </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>


    </div>
    <?php include ('includes/search-bar.php') ?>
    <?php include ('includes/footer.php') ?>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    
</body>
</html>