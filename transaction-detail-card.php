<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
<div class="theme-body">
    <?php include('includes/header.php') ?>

    <section>
        <div class="main-banner-wrap">
            <div class="main-banner-inner">
                <div class="banner-img" style="background-image: url(images/banner-desktop.jpg);">
                    <img src="images/transaction_history.png" class="main-banner-logo" alt="">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="reload-wrap mb-4">
            <div class="container">
                <div class="reload-inner row bg--gray-translucent border-radius-large pt-20 justify-content-center">
                    <div class="col-sm-12 col-md-12 col-xl-12">
                        <!-- <div class="text-center">
                            <h1>Transaction History</h1>
                        </div> -->
                        <div class="transaction-detail-wrap">
                            <h4> رقم الطلب D17687849</h4>
                            <div class="order-progress-bar">
                                <div class="order-progress-inner">
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-10">
                                            <div class="order-timeline">
                                                <div class="container">
                                                    <div class="progress-container">
                                                        <div class="progress" id="progress"></div>
                                                        <div class="step">
                                                            <div class="circle active">
                                                                <i class="fa fa-cart-plus"></i>   
                                                            </div>
                                                            <h6>تم  انشاء طلبك</h6>
                                                            <p>2020 12 23:00</p>
                                                        </div>
                                                        <div class="step">
                                                            <div class="circle">
                                                                <i class="fa fa-credit-card"></i>
                                                            </div>
                                                            <h6>تم الدفع</h6>
                                                            <p>2020 12 23:00</p>
                                                        </div>
                                                        <div class="step">
                                                            <div class="circle">
                                                                <i class="fa fa-truck"></i>   
                                                            </div>
                                                            <h6>توصيل الطلب</h6>
                                                            <p>2020 12 23:00</p>
                                                        </div>
                                                       <div class="step">
                                                            <div class="circle">
                                                                <i class="fa fa-check"></i>
                                                            </div>
                                                            <h6>تم طلبك بنجاح</h6>
                                                            <p>2020 12 23:00</p>
                                                       </div>
                                                    </div>
                                                </div>
                                                <p>شكرا لاستخدامك جيكسن !</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-10">
                                    <div class="transacton-detail-info">
                                        <p>من فضلك ابقا هذه الايصال معك من اجل ضمان عمليه الشحن الخاصه بك , هذه تفاصيل عمليتك</p>
                                        <div class="product-info">
                                            <p>بابجي باستخدام الايدي</p>
                                            <span>199LO Android EN Points</span>
                                        </div>
                                        <div class="transaction-info">
                                            <p>معلومات العمليه</p>
                                            <div class="trans-info-item">
                                                <h6>تاريخ العمليه </h6>
                                                <p>10/10/2021 5:35:56 AM (UTC)</p>
                                            </div>
                                            <div class="trans-info-item">
                                                <h6>رقم المعرف للعمليه</h6>
                                                <p>0322snrt0ndsk992</p>
                                            </div>
                                            <div class="trans-info-item">
                                                <p>رقم البطاقه : 7jjakallmmd89</p>
                                            </div>
                                            <div class="trans-info-item">
                                                <p>رقم البطاقه: 5897789220</p>
                                            </div>
                                            <div class="trans-info-item">
                                                <h6>طريقه الدفع</h6>
                                                <p> Gexin Gold  <img src="images/zgold.png" alt=""></p>
                                            </div>
                                            <div class="trans-info-item">
                                                <h6>قيمه الشحن</h6>
                                                <p>  1.99 <img src="images/zgold.png" alt=""></p>
                                            </div>

                                        </div>
                                        <!-- <div class="promiotion-wrap">
                                            <h6>Promotions eligible from this purchase</h6>
                                            <ul>
                                                <li><img src="images/zsilver.png" alt=""> spend Gold Earn Silver</li>
                                            </ul>
                                        </div> -->
                                        <div class="status">
                                            <h6>الحاله</h6>
                                            <p>ناجحه</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>



</div>
<?php include ('includes/search-bar.php') ?>
<?php include ('includes/footer.php') ?>
<?php include('includes/js.php') ?>
<script src="js/slick.min.js"></script>

</body>
</html>