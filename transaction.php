<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
<div class="theme-body">
    <?php include('includes/header.php') ?>

    <section>
        <div class="main-banner-wrap">
            <div class="main-banner-inner">
                <div class="banner-img" style="background-image: url(images/banner-desktop.jpg);">
                    <img src="images/transaction_history.png" class="main-banner-logo" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class=" d-lg-block d-xl-block d-md-block d-none rtl">
        <div class="reload-wrap mb-4">
            <div class="container">
                <div class="reload-inner row bg--gray-translucent border-radius-large pt-20 justify-content-center">
                    <div class="col-sm-12 col-md-12 col-xl-12">
                        <div class="text-center">
                            <h1>تاريخ المعاملات</h1>
                        </div>
                        <div class="transaction-tabs">
                            <div class="tabs">
                                <ul class="nav flex nav-pills nav-pills-custom" id="v-pills-tab" role="tablist">
                                    <li>
                                        <div class="icon-img">
                                            <img src="images/zsilver.png" alt="">
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-1 active" data-bs-toggle="tab" href="#myProperties"
                                           role="tab">الذهب</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link p-1" data-bs-toggle="tab" href="#shared" role="tab">Shared
                                            Properties</a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="tabs-panel">
                                <div class="portal-tabs-content">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        <div class="tab-pane fade rounded show active" id="myProperties" role="tabpanel"
                                             aria-labelledby="">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">تاريخ الطلب</th>
                                                    <th scope="col">ةصف الطلب</th>
                                                    <th scope="col">نوع الطلب</th>
                                                    <th scope="col">القيمه</th>
                                                    <th scope="col">رقم الطلب</th>
                                                    <th scope="col">الحاله</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                    <tr onclick="transactionWithId()">
                                                        <td>09-Nov-2021</td>
                                                        <td>المحفظة الذهبية الجديدة اربح 500 ذهب</td>
                                                        <td>حصل</td>
                                                        <td>+ 500</td>
                                                        <td>0422TY7VOWT37F2DE504E</td>
                                                        <td>النجاح</td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade rounded" id="shared" role="tabpanel"
                                             aria-labelledby="">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">تاريخ</th>
                                                    <th scope="col">وصف</th>
                                                    <th scope="col">نوع</th>
                                                    <th scope="col">كمية</th>
                                                    <th scope="col">رقم المعاملة</th>
                                                    <th scope="col">الحالة</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td colspan="6"> لا يوجد سجلات</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                    <nav class="navigation" aria-label="Page navigation">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<section class=" d-lg-none d-xl-none d-md-none d-block">
    <div class="transaction-history-mbl">
        <div class="container">
            <div class="mbl-transaction-inner">
                <h4>تاريخ المعاملات</h4>
                <div class="accordion-mbl">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                فضة
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="">ذهب</a>
                                    </li>
                                    <li>
                                        <a href="">فضة</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="transaction-table-mbl">
                    <div class="transaction-info-mbl">
                        <div class="info">
                            <h5>تاريخ</h5>
                            <p>09-Nov-2020</p>
                        </div>
                        <div class="info">
                            <h5>وصف</h5>
                            <p>المحفظة الذهبية الجديدة اربح 500 ذهب</p>
                        </div>
                        <div class="info">
                            <h5>رقم المعاملة</h5>
                            <p>0422TY7V2DE504E</p>
                        </div>
                        <div class="transaction-detail-btn">
                            <a class="transaction-detail-button" onclick="transactionWithId()">عرض التفاصيل</a>
                        </div>
                    </div>
                </div>
                <div class="pagination-mbl">
                    <nav class="navigation" aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('includes/search-bar.php') ?>
<?php include('includes/footer.php') ?>
<?php include('includes/js.php') ?>
<script src="js/slick.min.js"></script>

</body>
</html>