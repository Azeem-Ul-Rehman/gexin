<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gexin</title>
    <?php include ('includes/css.php') ?>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
</head>
<body>
    <div class="theme-body">
        <?php include ('includes/dark-header.php') ?>

        <div class="add-info-wrapper">
            <div class="add-info-inner">
                <div class="container">
                    <div class="link-wrap">
                        <div class="page-title">
                            <h3>هوية شخصية</h3>
                        </div>
                        <div class="add-form">
                            <form method="POST" autocomplete="off">
                                <div class="row">
                                    <!-- <div class="col-12">
                                        <div class="info-box mb-3">
                                            <h5>البريد المفعل الان </h5>
                                            <div class="input-box">
                                                <div class="input-holder"> ألبريد الان</div>
                                                <input type="email" class="form-control" id="currentEmail" aria-describedby="currentEmail" readonly value="johndoe@gexin.com">
                                            </div>
                                        </div>

                                    </div> -->
                                    <div class="col-12">
                                        <div class="info-box mb-3">
                                            <!-- <h5>CURRENT EMAIL</h5> -->
                                            <label for="NewEmail" class="form-label">معرف Razer الخاص بك هو لقبك الفريد. باستخدام معرف  الخاص بك ، ستتمكن من الوصول إلى الكثير من الأدوات والموارد الأساسية لاكتساب ميزة تنافسية ضد لاعبين آخرين.</label>
                                            <div class="input-box new-mail">
                                                <div class="input-holder">هويتك</div>
                                                <input type="text" class="form-control" id="NewEmail" aria-describedby="NewEmail" placeholder="johndoe@gexin.com" value="UserNameHere">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="form-btns">
                                    <a class="back" href="account.php">للخلف</a>
                                    <a class="next" href="account.php">التالي</a>
                                </div>
                            </form>
                        </div>
                        
                    </div>  
                </div>
            </div>
        </div>


        <?php include ('includes/main-footer.php') ?>
    </div>
    <?php include ('includes/js.php') ?>
    <script src="js/slick.min.js"></script>
    <script>
        var checkboxEle = $(".itemCheck");
        checkboxEle.next().hide();
        checkboxEle.click(function() {
            var panelDiv = $(this).next();
            if($(this).is(":checked")) {
                panelDiv.slideDown();
            } else {
                panelDiv.slideUp();
            }
        });
</script>
</body>
</html>